import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import NotFoundImage from 'assets/images/404.svg';

import 'css/404.css';

export default class NotFound extends Component {
    render() {
        const RandomQoute = [
            'You wanna get high?',
            'Grats. You broke it.',
            'You look lost...',
            "Don't forget to bring a towel!",
            'Whoops! What are you doing here?',
            'I have no idea what is going on'
        ];

        const max = RandomQoute.length;

        return (
            <div>
                <div className="NotFoundContainer">
                    <img alt="404" src={NotFoundImage} />
                    <p>{RandomQoute[Math.floor(Math.random() * Math.floor(max))]}</p>
                    <NavLink to="/">
                        <button className="button brand default large">Go Back to Funkytown!</button>
                    </NavLink>
                </div>
            </div>
        )
    }
}