import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';


import discord from 'assets/images/discord.svg';

export default class HomeTop extends Component {
    render() {
        return (
            <div className='HomeTop'>
                <div className='textWrapper'>
                    <h1>Smoke Up Your Discord Server!</h1>
                    <p>Towelie will try its best to bring enough for all its members!</p>
                </div>
                <div className='buttonsWrapper'>
                    <div className="buttonWrapper">
                        <NavLink to="/downloads">
                            <button type="button" className="button brand default large grow ">
                                <img src={discord} alt="discord" />
                                Add to Discord
                                    </button>
                        </NavLink>
                    </div>
                    <div className="buttonWrapper">
                        <NavLink to="/downloads">
                            <button type="button" className="button black default large grow">
                                Learn More
                                    </button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}