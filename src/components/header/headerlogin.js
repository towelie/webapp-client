import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import twitter from 'assets/images/twitter.svg';
import discord from 'assets/images/discord.svg';

export default class HeaderLogin extends Component {
    render() {
        let { mobileMenuIsActive } = this.props
        return (
            <ul className='desktopHeaderRightNav'>
                <li className='desktopHeaderRightNavItem'>
                    <NavLink to="#">
                        <img src={twitter} alt='twitter' ></img>
                    </NavLink>
                </li>
                <li className='desktopHeaderRightNavItem'>
                    <NavLink to="#">
                        <img src={discord} alt='discord' style={{width: '17px'}}></img>
                    </NavLink>
                </li>
                <li className='desktopHeaderRightNavItem'>
                    <button className={`button white normal ${mobileMenuIsActive ? "flat" : "ghost rounded"}`}>Login</button>
                </li>
            </ul>
        )
    }
}