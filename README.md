<h1 align="center">Towelie</h1>

<div align="center">
  <strong>Towelie Discord Bot</strong>
</div>
<div align="center">
  A Discord <code>Bot</code> to manage your server to its best abilities 
</div>

<br />

<div align="center">
  <!-- NPM version -->
  <a href="https://nodejs.org/en/blog/release/v6.5.0/">
    <img src="https://img.shields.io/badge/npm-6.5.0-blue.svg"
      alt="NPM version" />
  </a>
  <!-- Build Status -->
  <a href="https://gitlab.com/towelie/webapp-client/pipelines">
    <img src="https://gitlab.com/towelie/webapp-client/badges/master/build.svg"
      alt="Build Status" />
  </a>
  <!-- Test Coverage -->
  <a href="https://gitlab.com/towelie/webapp-client/pipelines">
    <img src="https://gitlab.com/towelie/webapp-client/badges/develop/coverage.svg"
      alt="Test Coverage" />
  </a>
  <!-- Standard -->
  <a href="https://standardjs.com">
    <img src="https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat-square"
      alt="Standard" />
  </a>
</div>

<div align="center">
  <sub>The little project that could. Built with ❤︎ by
  <a href="https://twitter.com/codevski">Codevski</a> and
  <a href="#">
    contributors
  </a>
</div>

## License
[Copyright](https://gitlab.com/towelie/webapp-client/blob/master/LICENSE.md)
